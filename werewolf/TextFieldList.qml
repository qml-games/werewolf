import Qt 4.7

Rectangle{
	id : lvRect
	height: 420;
	width: 400;
	color: light ? "white" : "black";
	
	property alias model : lv.model;
	property bool light : false;
	
	ListView {
		id : lv
		anchors.fill : parent;
		anchors.margins : 3;
		clip: true;
		boundsBehavior: Flickable.StopAtBounds;
		
		
		model: ListModel{
			id : lmodel
		}
		
		
		
		delegate: Component {
			Rectangle{
				height: tf.height + 2;
				anchors.right : parent.right;
				anchors.left : parent.left;
				property bool even: (index % 2)
				color:  (even ? (!light?  "white" : "black") : (!light?   "lightblue" :"darkblue"));
				
				
				Text { text: listText ; width: 150;textFormat : Text.PlainText;font.pixelSize : 16
					color: light ? "white" : "black"; anchors.leftMargin: 6; anchors.left : parent.left;
					anchors.verticalCenter : parent.verticalCenter;
				}
				
				TextField {
					id : tf
					onTextChanged : lmodel.setProperty(index,"tfText",text)
					width: 50
					anchors.right : parent.right
					//we don't want property binding in this direction:
					Component.onCompleted : text = tfText;
					
// 					MouseArea {
// 						anchors.fill: parent
// 						onClicked: {console.log("juhu"+text);lmodel.setProperty(index,"tfText",text)}
// 					}
				}


			}
		}
		
	}
}
