import Qt 4.7

Rectangle {
	id: button
// 	color : "black";
	radius : 2;
	width: lbl.width + 10;
	height: 1.2 * lbl.font.pixelSize + 10;
	
	property bool light : true;
	property bool active : true;
	
	gradient: Gradient {
		GradientStop { position: 0.4; color: light ? "white" : "black" }
		GradientStop { position: 0.85; color: light ? "grey" : "darkgrey" }
		GradientStop { position: 0.95; color: light ? "white" : "black" }
	} 

	
	property alias text : lbl.text; 
	signal clicked();
	
	Text {
		id: lbl
		color: active? (light ? "black" : "white") : "gray";
		anchors.horizontalCenter: parent.horizontalCenter;
		anchors.verticalCenter : parent.verticalCenter;
		font.pointSize : 16;
		text: "Button"
	}
	MouseArea {
		anchors.fill: parent
		onClicked: if(active) button.clicked();
	}
} 
