import QtQuick 1.1


/* 
*/

Rectangle {
	id : main
	width: 800
	height: 480
	color: day ? "lightyellow" : "darkblue";
	anchors.fill : parent;
	clip : true;
	property bool day: false;
	property bool portrait: false;
	
	states: 
		State {
			name: "portrait"
			PropertyChanges { target: main; width: 460; height: 800 }
			PropertyChanges { target: gr; columns: 1 }
		}
		
	state: runtime.portrait ?  'portrait' : '';
		
	signal loadRequest(variant file);

	
	Grid{
		id: gr
		columns : 2
		rows: 4
		anchors.horizontalCenter: parent.horizontalCenter;
		anchors.verticalCenter : parent.verticalCenter;
		spacing: 40
		
		Rectangle{
			height: 100
			width: 100
			radius: 50
			rotation: 60
			gradient: Gradient {
				GradientStop { position: 0.6; color: "white" }
				GradientStop { position: 0.95; color: "darkblue" }
			}
		}
		
		Text {
			text: "Werewolf"
			font.pointSize : 48
			height: 200
			color: "red"
		}
		
		Button {
			//anchors.horizontalCenter: parent.horizontalCenter;
			text: "Create new game"
			light : !day 
			onClicked :  main.loadRequest("Server.qml");

		}
		
		Button {
			//anchors.horizontalCenter: parent.horizontalCenter;
			text: "Search host"
			light : !day 
			onClicked : main.loadRequest("Client.qml");
		}
		
	}
}
