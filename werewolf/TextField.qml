import Qt 4.7

Rectangle {
	id: button
// 	color : "black";
	radius : 2;
	border.width:2;
	width: lbl.font.pixelSize * 10;
	height: 1.2 * lbl.font.pixelSize + 10;

	
	property bool light : true;
	
	color: light ? "white" : "black";

	
	property alias text : lbl.text; 
	signal clicked();
	
	TextEdit {
		id: lbl
		color: light ? "black" : "white";
		anchors.verticalCenter : parent.verticalCenter;
		x : 6;
		font.pointSize : 16;
		text: "Button"
	}
// 	MouseArea {
// 		anchors.fill: parent
// 		onClicked: button.clicked();
// 	}
} 
