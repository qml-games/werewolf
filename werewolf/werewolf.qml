import QtQuick 1.1
import simpleqmlrunner 1.0
import qmlnetgame 1.0
import "script.js" as JS



RUntime{
	id: runtime
	property bool portrait: false;
	
	property RWindow mainWdw : RWindow {
		id : mainWdw
		title: "Werewolf"
		fullscreen : false;
		stacked: true;
		width: 800
		height: 460
		anchors.margins : 0
		
		function init(){}
		
		function loadContent(file){
			mainWdwLd.source = file;
			if(mainWdwLd.item.loadRequest){
				mainWdwLd.item.loadRequest.connect(loadContent);
			}
			if(mainWdwLd.item.init){
				mainWdwLd.item.init(JS);
			}
		}

		Loader {
			id: mainWdwLd
			anchors.fill: parent
		}
	}
	
	property Communicator comm : Communicator {
		property int port : 2000;
		property bool server;
		onMessageReceived: JS.messageReceived(msg,sendId);
		onUdpMessageReceived: JS.udpMessageReceived(msg,host);
		onClientAvailable: JS.clientAvailable();
	}
	
	onReady: {
		mainWdw.show();
		mainWdw.loadContent("Choice.qml");
	}
	
	onRotationChanged: portrait = (rot == 1) ? true : false; 
}

