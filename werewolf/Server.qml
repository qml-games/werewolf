import QtQuick 1.1


/* 
*/

Rectangle {
	id : main
	width: 800
	height: 480
	color: day ? "lightyellow" : "darkblue";
	anchors.fill : parent;
	clip : true;
	property bool day: false;
	property bool portrait: false;
	
	states: 
		State {
			name: "portrait"
			PropertyChanges { target: main; width: 460; height: 800 }
			PropertyChanges { target: outerGr; columns: 1 }
		}
		
	state: runtime.portrait ?  'portrait' : '';
	
	signal loadRequest(variant file);
	signal startBroadcast
	signal startServer
	signal startGame
	
	function init(JS){
		startBroadcast.connect(JS.startBroadcast);
		startGame.connect(JS.startGame);
		JS.startBroadcast();
		JS.startServer();
// 		startServer.connect(JS.startServer);
	}
	
	Component.onCompleted : { 
		main.startBroadcast();
		main.startServer();
	}
	
	function listClient(name,addr) {
		clientList.model.append({
			"name" : name,
			"addr" : addr,
			"listText" : name + " on " + addr
		});
	}
			
	Grid {
		id: outerGr
		columns: 2;
		rows: 1;
		spacing: 10;
		anchors.fill: parent;
		anchors.margins: 5;
			
		SimpleList {
			id: clientList
			light : !day;
		}

		Grid{
			id: gr
			columns : 2
			rows: 4
			spacing: 40
			
			Button {
				text: "Start Game"
				light : !day 
				onClicked : main.startGame();
			}
			
			Button {
				text: "Restart Brodcasting"
				light : !day 
				onClicked : main.startBroadcast();
			}
		}
	}
}
