import QtQuick 1.1


/* 
*/

Rectangle {
	id : main
	width: 800
	height: 480
	color: day ? "lightyellow" : "darkblue";
	anchors.fill : parent;
	clip : true;
	property bool day: false;
	property bool portrait: false;
	property int numOfPlayers : 0;
	property alias roleListModel : roleList.model;
	
	states: 
		State {
			name: "portrait"
			PropertyChanges { target: main; width: 460; height: 800 }
			PropertyChanges { target: outerGr; columns: 1 }
		}
		
	state: runtime.portrait ?  'portrait' : '';
	
	signal submitRoles
	
	function init(JS){
		submitRoles.connect(JS.submitRoles);
		for(var i=0;i < JS.roles.length;i++)
			roleList.model.append(JS.roles[i]);
	}
	
	function sumUpRoles(){
		var sum = 0;
		for(var i=0;i < roleList.model.count;i++){
			var j = parseInt(roleList.model.get(i).tfText);
			if(j)
				sum += j;
		}
		return sum;
	}
	Component.onCompleted : { 

	}
	

			
	Grid {
		id: outerGr
		columns: 2;
		rows: 1;
		spacing: 10;
		anchors.fill: parent;
		anchors.margins: 5;
			
		TextFieldList {
			id: roleList
			light : !day;

			}

		Grid{
			id: gr
			columns : 1
			rows: 4
			spacing: 40
			
			Button {
				text: "Submit Characters"
				light : !day 
				active : sumUpRoles() == numOfPlayers;
				onClicked : main.submitRoles();

			}
			
			Label {
				text: "Number of Players: " + numOfPlayers + " Sum: " + sumUpRoles();
				light: !day;
			}
			
		}
	}
}
