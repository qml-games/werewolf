var currentAck = function(){}
var knownServers = {};
var srvId = 0;
var myIdAtServer = -1;
var clIds = new Array();
var players = new Array();
var allPlayersCounter = 0;
var roleInformationFinishedCounter = 0;
var localPlayers = new Array();
var loopCounter = 0;
var playersByRoles= {};//playersByRole["role"] is an array of indices of players who have that role

function startBroadcast(){
	comm.broadcastExistence("Werewolf-Server",1000,30000,comm.port);
}

function startGame(){
	console.log(JS.clIds.length);
	for(var i =0; i< JS.clIds.length;i++)
		comm.requestSend("accepted:" + JS.clIds[i], JS.clIds[i]);
	mainWdw.loadContent("PlayerSelect.qml");
}

function startServer(){
	//a non-zero srvPort makes Comminicator start a server
	comm.srvPort = comm.port;
}

function submitPlayers(){
	//after connection we use this to test the role
	if(JS.myIdAtServer == -1)/*Server*/{
		for(var i = 0;i < mainWdwLd.item.playerListModel.count; i++){
			var pl = {
				"comId" : -1,
				"playerId" : i,
				"name" : mainWdwLd.item.playerListModel.get(i).name
			};
			JS.players.push(pl);
			JS.localPlayers.push(pl);
		}
		++allPlayersCounter;
		if(allPlayersCounter == clIds.length + 1){
			for(var i =0; i< JS.clIds.length;i++)
				comm.requestSend("roleSelection:" + JS.clIds[i], JS.clIds[i]);
			startRoleSelection();
			return;
		}
	} else {
		for(var i = 0;i < mainWdwLd.item.playerListModel.count; i++){
			comm.requestSend("addPlayer:" +i+":" +mainWdwLd.item.playerListModel.get(i).name, JS.svrId);
			JS.localPlayers.push({
				"comId" : -1,
				"playerId" : i,
				"name" : mainWdwLd.item.playerListModel.get(i).name
			});
		}
		comm.requestSend("allPlayersAdded:", JS.svrId);
	}
	mainWdw.loadContent("Wait.qml");
	mainWdwLd.item.message = "Please wait until all players are connected";
}

function startRoleSelection(){
	mainWdw.loadContent("RoleSelect.qml");
	mainWdwLd.item.numOfPlayers = players.length;
}

function submitRoles(){
	var choosenRoles = new Array();
	//create an array for every choosen role 
	for(var i =0;i < mainWdwLd.item.roleListModel.count;i++)
		if(parseInt(mainWdwLd.item.roleListModel.get(i).tfText))
			playersByRoles[mainWdwLd.item.roleListModel.get(i).name] = new Array();
	//create n entries for n characters of type role (=> sum = players.length)
	for(var i =0;i < mainWdwLd.item.roleListModel.count;i++)
		for(var j =0; j < parseInt(mainWdwLd.item.roleListModel.get(i).tfText);j++)
			choosenRoles.push({
				"type" : mainWdwLd.item.roleListModel.get(i).name,
				"name" : mainWdwLd.item.roleListModel.get(i).name + j
			});
	//choose randomly and remove the choosen character
	for(var i = 0;i < JS.players.length; i++){
		var r = Math.floor(Math.random() * choosenRoles.length);
		if(r == choosenRoles.length)
			r = r-1;
		JS.players[i].role = choosenRoles[r];
		choosenRoles.splice(r,1);
		JS.playersByRoles[JS.players[i].role.name] = i;
	}
	for(var i =0;i < JS.players.length;i++)
		if(JS.players[i].comId == -1)
			JS.localPlayers[JS.players[i].playerId].role = JS.players[i].role;
		else 
			comm.requestSend("playerGotRole:"+JS.players[i].playerId+":"+players[i].role.type+":"+
				JS.players[i].role.name, JS.clIds[players[i].comId]);
	for(var i =0; i< JS.clIds.length;i++)
		comm.requestSend("showRolesToPlayers:", JS.clIds[i]);
	mainWdw.loadContent("Wait.qml");
	startRoleInformation();
	//TODO: send roles to clients
}

function startRoleInformation(){
	if(!JS.localPlayers.length)
		roleInformationFinished();
	currentAck = nextRoleInformation;
	mainWdw.loadContent("Hello.qml");
	mainWdwLd.item.playerName = JS.localPlayers[0].name;
	loopCounter = 0;
}

function nextRoleHello(){
	if(JS.loopCounter >= JS.localPlayers.length-1)
		roleInformationFinished();
	JS.loopCounter++;
	currentAck = nextRoleInformation;
	mainWdw.loadContent("Hello.qml");
	mainWdwLd.item.playerName = JS.localPlayers[JS.loopCounter].name;
}

function nextRoleInformation(){
	currentAck = nextRoleHello;
	mainWdw.loadContent(JS.localPlayers[JS.loopCounter].role.type + "_desc.qml");
}

function roleInformationFinished(){
	if(JS.myIdAtServer == -1)/*Server*/{
		JS.roleInformationFinishedCounter++;
		if(JS.roleInformationFinishedCounter == JS.clIds.length +1)
			startNextNight(1);
	} else {
		comm.requestSend("roleInformationFinished:", JS.svrId);
	}
}

function startNextNight(number){
	syncPlayersAlive();
}

function syncPlayersAlive(){
}

function messageReceived(msg,sendId){
	console.log(msg);
	var sepIdx = msg.indexOf(":");
	var msgClass = msg.substring(0,sepIdx);
	var msgBody = msg.substring(sepIdx+1);
	switch(msgClass){
	case "join":
		if(comm.srvPort)
			mainWdwLd.item.listClient(msgBody,sendId);
		break;
	case "accepted":
		JS.myIdAtServer = parseInt(msgBody);
		mainWdw.loadContent("PlayerSelect.qml");
		break;
	case "addPlayer":
		sepIdx = msgBody.indexOf(":");
		var localNum = parseInt(msgBody.substring(0,sepIdx));
		var name = msgBody.substring(sepIdx+1);
		JS.players.push({
			"comId" : sendId,
			"playerId" : localNum,
			"name" : name
		});
		break;
	case "allPlayersAdded":
		++allPlayersCounter;
		if(allPlayersCounter == clIds.length + 1){
			for(var i =0; i< JS.clIds.length;i++)
				comm.requestSend("roleSelection:" + JS.clIds[i], JS.clIds[i]);
			startRoleSelection();
		}
		break;
	case "roleSelection":
		mainWdwLd.item.message = "Choose the characters on the server!";
		break;
	case "playerGotRole":
		sepIdx = msgBody.indexOf(":");
		var playerId = parseInt(msgBody.substring(0,sepIdx));
		msgBody = msgBody.substring(sepIdx+1);
		sepIdx = msgBody.indexOf(":");
		var roleType = msgBody.substring(0,sepIdx);
		var roleName = msgBody.substring(sepIdx+1);
		JS.localPlayers[playerId].role = {
			"type" : roleType,
			"name" : roleName
		};
		break;
	case "showRolesToPlayers":
		startRoleInformation();
		break;
	case "roleInformationFinished":
		++roleInformationFinishedCounter;
		if(roleInformationFinishedCounter == clIds.length + 1){
			startNextNight(1);
		}
		break;
	}
}



function udpMessageReceived(msg,host){
	if(!knownServers[host]){
		knownServers[host] = true;
		mainWdwLd.item.listServer(msg,host);
	}
}

function clientAvailable(){
	JS.clIds.push(comm.acceptClient());
}

function connectToHost(host){
	srvId = comm.addConnectionToServer(host,comm.port);
	comm.requestSend("join: Werewolf " + host, JS.svrId);
}



var roles = [
	{ "name" : "Wolf","listText" : "Wolf(s)","tfText":"0"},
	{ "name" : "Citizen","listText" : "Citizen(s)","tfText":"0"},
	{ "name" : "Witch","listText" : "Witch(es)","tfText":"0"},
	{ "name" : "Hunter","listText" : "Hunter(s)","tfText":"0"},
	{ "name" : "Wolf","listText" : "Wolf(s)","tfText":"0"},
	{ "name" : "Wolf","listText" : "Wolf(s)","tfText":"0"},
	{ "name" : "Wolf","listText" : "Wolf(s)","tfText":"0"},
	{ "name" : "Wolf","listText" : "Wolf(s)","tfText":"0"},
	{ "name" : "Wolf","listText" : "Wolf(s)","tfText":"0"},
	{ "name" : "Wolf","listText" : "Wolf(s)","tfText":"0"},
	{ "name" : "Wolf","listText" : "Wolf(s)","tfText":"0"},
	{ "name" : "Wolf","listText" : "Wolf(s)","tfText":"0"},
	{ "name" : "Wolf","listText" : "Wolf(s)","tfText":"0"},
	{ "name" : "Wolf","listText" : "Wolf(s)","tfText":"0"},
	{ "name" : "Wolf","listText" : "Wolf(s)","tfText":"0"},
	{ "name" : "Wolf","listText" : "Wolf(s)","tfText":"0"},
	{ "name" : "Wolf","listText" : "Wolf(s)","tfText":"0"}
];