import QtQuick 1.1

/* 
*/

Rectangle {
	id : main
	width: 800
	height: 480
	color: day ? "lightyellow" : "darkblue";
	anchors.fill : parent;
	clip : true;
	property bool day: false;
	property bool portrait: false;
	property string playerName: "muhu";
	
	states: 
		State {
			name: "portrait"
			PropertyChanges { target: main; width: 460; height: 800 }
			PropertyChanges { target: outerGr; columns: 1 }
		}
		
	state: runtime.portrait ?  'portrait' : '';
	
	signal ack
	
	function init(JS){
		ack.connect(JS.currentAck);
	}
	
	Component.onCompleted : { 

	}
			


	Grid{
		id: gr
		columns : 1
		rows: 2
		spacing: 40
		
		Label {
			text: "Hello " + main.playerName + ", click OK, if only you can see the screen."
		}
		
		Button {
			text: "OK"
			light : !day 
			onClicked : main.ack();

		}
		
	}

}
