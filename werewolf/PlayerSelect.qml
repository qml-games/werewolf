import Qt 4.7

/* 
*/

Rectangle {
	id : main
	width: 800
	height: 480
	color: day ? "lightyellow" : "darkblue";
	anchors.fill : parent;
	clip : true;
	property bool day: false;
	property bool portrait: false;
	property int myIdAtServer;
	property alias playerListModel : playerList.model;
	
	states: 
		State {
			name: "portrait"
			PropertyChanges { target: main; width: 460; height: 800 }
			PropertyChanges { target: outerGr; columns: 1 }
		}
		
	state: runtime.portrait ?  'portrait' : '';
	
	signal submitPlayers
	
	function init(JS){
		myIdAtServer = JS.myIdAtServer;
		submitPlayers.connect(JS.submitPlayers);
	}
	
	Component.onCompleted : { 

	}
	
	function listPlayer(name) {
		playerList.model.append({
			"name" : name,
			"listText" : name + " on " + 
				((myIdAtServer == -1) ? "Server" : ("Client " + myIdAtServer))
		});
	}
			
	Grid {
		id: outerGr
		columns: 2;
		rows: 1;
		spacing: 10;
		anchors.fill: parent;
		anchors.margins: 5;
			
		SimpleList {
			id: playerList
			light : !day;
		}

		Grid{
			id: gr
			columns : 1
			rows: 4
			spacing: 40
			
			Button {
				text: "submit players"
				light : !day 
				onClicked : main.submitPlayers();

			}
			
			Button {
				text: "add Player"
				light : !day 
				onClicked : {
					listPlayer(nameTf.text);
				}
			}
			
			TextField {
				id : nameTf
				text: "Player's name"
			}
		}
	}
}
