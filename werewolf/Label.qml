import Qt 4.7

Rectangle {
	id: backgr
	color: "transparent";
	radius : 2;
	width: lbl.width + 10;
	height: 1.2 * lbl.font.pixelSize + 10;
	
	property bool light : true;

	
	property alias text : lbl.text; 
	signal clicked();
	
	Text {
		id: lbl
		color: light ?  "white" : "black";
		anchors.horizontalCenter: parent.horizontalCenter;
		anchors.verticalCenter : parent.verticalCenter;
		font.pointSize : 16;
		text: "Button"
	}
} 
