import Qt 4.7

/* 
*/

Rectangle {
	id : main
	width: 800
	height: 480
	color: day ? "lightyellow" : "darkblue";
	anchors.fill : parent;
	clip : true;
	property bool day: true;
	property bool portrait: false;
	
	states: 
		State {
			name: "portrait"
			PropertyChanges { target: main; width: 460; height: 800 }
			PropertyChanges { target: outerGr; columns: 1 }
		}
		
	state: runtime.portrait ?  'portrait' : '';
	
	signal loadRequest(variant file);
	signal connectToHost(string host);
	
	function init(JS){
		connectToHost.connect(JS.connectToHost);
	}
	
	Component.onCompleted : {
		comm.listenForBroadcast(2000);
	}
	
	function listServer(name,addr) {
		serverList.model.append({
			"name" : name,
			"addr" : addr,
			"listText" : name + " on " + addr
		});
	}
			
	Grid {
		id: outerGr
		columns: 2;
		rows: 1;
		spacing: 10;
		anchors.fill: parent;
		anchors.margins: 5;
			
		SimpleList {
			id: serverList;
			light : !day;
		}

		Grid{
			id: gr
			columns : 1
			rows: 4
			spacing: 40
			
			Button {
				text: "Connect to selected Host"
				light : !day 
				onClicked : main.connectToHost(serverList.model.get(serverList.currentIndex).addr);
			}
			
			Button {
				text: "Restart Brodcasting"
				light : !day 
			}
		}
	}
}
