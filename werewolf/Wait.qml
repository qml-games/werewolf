import Qt 4.7


/* 
*/

Rectangle {
	id : main
	width: 800
	height: 480
	color: day ? "lightyellow" : "darkblue";
	anchors.fill : parent;
	clip : true;
	property bool day: false;
	property bool portrait: false;
	property alias message : waitText.text
	
	states: 
		State {
			name: "portrait"
			PropertyChanges { target: main; width: 460; height: 800 }
			PropertyChanges { target: gr; columns: 1 }
		}
		
	state: runtime.portrait ?  'portrait' : '';

	
	Grid{
		id: gr
		columns : 2
		rows: 4
		anchors.horizontalCenter: parent.horizontalCenter;
		anchors.verticalCenter : parent.verticalCenter;
		spacing: 40
		
		Rectangle{
			height: 100
			width: 100
			radius: 50
			rotation: 60
			gradient: Gradient {
				GradientStop { position: 0.6; color: "white" }
				GradientStop { position: 0.95; color: "darkblue" }
			}
		}
		
		Text {
			id : waitText
			text: ""
			font.pointSize : 16
			height: 200
			color: day ? "black" : "white"
		}
		
		
	}
}
