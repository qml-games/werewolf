import Qt 4.7

Rectangle{
	id : lvRect
	height: 420;
	width: 400;
	color: light ? "white" : "black";
	
	property alias model : lv.model;
	property bool light : false;
	
	ListView {
		id : lv
		anchors.fill : parent;
		anchors.margins : 3;
		clip: true;
		boundsBehavior: Flickable.StopAtBounds;
		
		
		model: ListModel{
			id : lmodel
		}
		
		
		highlight: Rectangle { color: "orange"; radius: 2 ; width: 10; height: 20}
		
		delegate: Component {
			Rectangle{
				height: 20;
				anchors.right : parent.right;
				anchors.left : parent.left;
				property bool even: (index % 2)
				color: ListView.isCurrentItem ? (!light ? "orange":"darkred") : (even ? (!light?  "white" : "black") : (!light?   "lightblue" :"darkblue"));
				
				
				Text { text: listText ; width: 150;textFormat : Text.PlainText;font.pixelSize : 16
					color: light ? "white" : "black"; x: 6; 
				}

				MouseArea {
					anchors.fill: parent
					onClicked: {lv.currentIndex = index;}
				}
			}
		}
		
	}
}
